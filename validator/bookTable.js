
exports.errorFunction = (req, res, next) => {
  const errors = req.validationErrors();
  if (errors) {
      const firstError = errors.map(error => error.msg)[0];
      return res.status(400).json({ error: firstError });
  }
  next();
}
exports.bookTableValidator = (req, res, next) => {
  req.check('name', 'Enter your name').notEmpty();
  req.check('name', 'name must be 3 character or more').isLength({
      min: 3,
      max: 50
  })
  req.check('cust_id', 'Enter the customer id').notEmpty();
  req.check('seats', 'Enter total number of guests').notEmpty();
  req.check('time_slot', 'Select time to book a table').notEmpty();
  req.check('date_slot', 'Pick a date to book a table').notEmpty();
  req.check('email', 'please enter email id').notEmpty();
  req.check('email', 'Please enter proper email id').isEmail();
  req.check('mobile', 'Please enter mobile number').notEmpty();
  req.check('mobile', 'Please enter valid mobile number').isLength({
      min: 10,
      max: 10
  });
  req.check('status', 'Please enter status').notEmpty();

  this.errorFunction(req, res, next);
}