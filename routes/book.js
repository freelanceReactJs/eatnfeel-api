const router = require('express').Router();
const {
    bookTable,
    getBookTable,
    tableById,
    updateBooking,
    deleteBooking,
    cancelBooking,
    viewCancelBookings,
    viewBookedBookings,
    viewTodaysBookings
} = require('../controllers/book');
const { bookTableValidator } = require('../validator/bookTable');

router.post('/book/table', bookTableValidator, bookTable);
router.get('/booking', getBookTable);
router.put('/book/:tableById', updateBooking);
router.delete('/book/:tableById', deleteBooking);
router.put('/book/cancel/:tableById', cancelBooking);
router.get('/booking/cancel', viewCancelBookings);
router.get('/booking/booked', viewBookedBookings);
router.get('/booking/today', viewTodaysBookings);

router.param('tableById', tableById);
module.exports = router;