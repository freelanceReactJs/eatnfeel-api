const mongoose = require('mongoose');

const book = mongoose.Schema({
    name: {
        type: String,
    },
    cust_id: {
        type: String,
    },
    seats: {
        type: Number
    },
    time_slot: {
        type: String
    },
    date_slot: {
        type: String
    },
    email: {
        type: String
    },
    mobile: {
        type: Number
    },
    status: {
        type: String
    }
});

module.exports = mongoose.model('book', book);