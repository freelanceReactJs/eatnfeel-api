const book = require('../models/bookModel');
const _ = require('lodash');

//for user
exports.bookTable = (req, res) => {
    const { name, cust_id, seats, time_slot, date_slot, email, mobile, status } = req.body;
    const Book = new book({
        name,
        cust_id,
        seats,
        time_slot,
        date_slot,
        email,
        mobile,
        status
    });
    Book.save((err, data) => {
        if (err)
            res.status(400).json({ error: err });

        res.json({ data: data });
    })
}

//for admin
exports.getBookTable = (req, res) => {
    book
        .find()
        .exec((err, data) => {
            if (err)
                res.status(400).json({ error: err });

            res.json(data);
        })
}

exports.tableById = (req, res, next, id) => {
    book.findById(id).exec((err, data) => {
        if (err || !data) {
            return res.status(400).json({
                error: "booking not found"
            });
        }
        req.bookTables = data;
        next();
    });
};

//for user
exports.updateBooking = (req, res) => {
    let book = req.bookTables;
    book = _.extend(book, req.body);
    book.save((err, data) => {
        if (err)
            res.status(400).json({ error: err });

        res.json({ data: data });
    });
}

//for admin
exports.deleteBooking = (req, res) => {
    let book = req.bookTables;
    book.remove((err, user) => {
        if (err) {
            res.status(400).json({
                error: err
            });
        }

        res.json({ status: "success", message: "booking deleted successfully" });
    });
}

//for user
exports.cancelBooking = (req, res) => {
    let book = req.bookTables;
    book = _.extend(book, req.body);
    book.save((err, data) => {
        if (err) {
            res.status(400).json({
                error: err
            });
        }

        res.json({ message: "booking cancelled successfully" });
    })
}

exports.viewCancelBookings = (req, res) => {
    book.find({ status: "cancelled" })
        .exec()
        .then(data => {
            res.json(data)
        })
        .catch(error => {
            res.status(400).json({ error: error });
        })
}

exports.viewBookedBookings = (req, res) => {
    book.find({ status: "booked" })
        .exec()
        .then(data => {
            res.json(data)
        })
        .catch(error => {
            res.status(400).json({ error: error });
        })
}

exports.viewTodaysBookings = (req, res) => {
    book.find({ date_slot: new Date().toISOString().substring(0,10) })
        .exec()
        .then(data => {
            console.log(new Date().toLocaleDateString());

            res.json(data)
        })
        .catch(error => {
            res.status(400).json({ error: error });
        })
}